<%-- 
    Document   : map1
    Created on : 17 Apr, 2017, 2:38:05 PM
    Author     : akhil
--%>
<%@page import="java.sql.ResultSet"%>
<%@page import="java.sql.Statement"%>
<%@page import="dbclass.dbconne"%>

<html class="no-js">
    <head>
        <!-- Basic Page Needs
        ================================================== -->
        <meta charset="utf-8">
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
        <link rel="icon" type="image/png" href="images/favicon.png">
        <title>Banking Bot</title>
        <meta name="description" content="">
        <meta name="keywords" content="">
        <meta name="author" content="">
        <!-- Mobile Specific Metas
        ================================================== -->
        <meta name="format-detection" content="telephone=no">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        
        <!-- Template CSS Files
        ================================================== -->
        <!-- Twitter Bootstrs CSS -->
        <link rel="stylesheet" href="css/bootstrap.min.css">
        <link href="css/table.css" rel="stylesheet">
        <!-- Ionicons Fonts Css -->
        <link rel="stylesheet" href="css/ionicons.min.css">
        <!-- animate css -->
        <link rel="stylesheet" href="css/animate.css">
        <!-- Hero area slider css-->
        <link rel="stylesheet" href="css/slider.css">
        <!-- owl craousel css -->
        <link rel="stylesheet" href="css/owl.carousel.css">
        <link rel="stylesheet" href="css/owl.theme.css">
        <link rel="stylesheet" href="css/jquery.fancybox.css">
        <!-- template main css file -->
        <link rel="stylesheet" href="css/main.css">
        <!-- responsive css -->
        <link rel="stylesheet" href="css/responsive.css">
        
        <!-- Template Javascript Files
        ================================================== -->
        <!-- modernizr js -->
        <script src="js/vendor/modernizr-2.6.2.min.js"></script>
        <!-- jquery -->
        <script src="//ajax.googleapis.com/ajax/libs/jquery/1.10.2/jquery.min.js"></script>
        <!-- owl carouserl js -->
        <script src="js/owl.carousel.min.js"></script>
        <!-- bootstrap js -->

        <script src="js/bootstrap.min.js"></script>
        <!-- wow js -->
        <script src="js/wow.min.js"></script>
        <!-- slider js -->
        <script src="js/slider.js"></script>
        <script src="js/jquery.fancybox.js"></script>
        <!-- template main js -->
        <script src="js/main.js"></script>
        
    </head>
    <body>
        <!--
        ==================================================
        Header Section Start
        ================================================== -->
        <header id="top-bar" class="navbar-fixed-top animated-header">
            <div class="container">
                <div class="navbar-header">
                    <!-- responsive nav button -->
                    <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    </button>
                    <!-- /responsive nav button -->
                    
                    <!-- logo -->
                    <div class="navbar-brand">
                        <a href="index.html" >
                            <img src="images/bba.png" alt="">
                        </a>
                    </div>
                    <!-- /logo -->
                </div>
                <!-- main menu -->
                        <nav class="collapse navbar-collapse navbar-right" role="navigation">
                    

                            <div class="main-menu">
                        <ul class="nav navbar-nav navbar-right">
                                                         <li>
                                <a href="../contact.html" >Contact</a>
                            </li>
                            <li><a href="../about.html">About</a></li>
                            <li><a href="../talkiee2.jsp">Search your Query </a></li>
                            
                            <li><a href="../feedback.jsp">FeedBack</a></li>
                            <li><a href="../newsfeed.jsp">LatestNews</a></li>
                            <li><a href ="../chat/chat.jsp">Chat here</a></li>
                            <li><a href ="logout.jsp">Logout</a><li> 
                           
                            
                        </ul>
                    </div>
                </nav>
                <!-- /main nav -->
            </div>
        </header>
<style>
      
      #map {
        height: 95%;
		background-color:#666666;
      }
    </style>
<script>
/*
function getLocation() {
    if (navigator.geolocation) {
        navigator.geolocation.getCurrentPosition(showPosition);
    } else { 
        x.innerHTML = "Geolocation is not supported by this browser.";
    }
}

function showPosition(position) {
    var x=position.coords.latitude;	
    document.getElementById("lat").value = x;
    var x=position.coords.longitude;	
    document.getElementById("log").value = x;
}*/
</script>

	    <script>
// This example requires the Places library. Include the libraries=places
// parameter when you first load the API. For example:
// <script src="https://maps.googleapis.com/maps/api/js?key=YOUR_API_KEY&libraries=places">

var map;
var infowindow;

function initMap() {
  var latlngStr = document.getElementById('lat').value;
  var latlngStr1 = document.getElementById('log').value;
  var pyrmont = {lat: parseFloat(latlngStr), lng: parseFloat(latlngStr1)};


 
  map = new google.maps.Map(document.getElementById('map'), {
    center: pyrmont,
    zoom: 15
  });


  infowindow = new google.maps.InfoWindow();

  var service = new google.maps.places.PlacesService(map);
  service.nearbySearch({
    location: pyrmont,
    radius: 1000,
    types: ['<%=request.getParameter("search")%>']
  }, callback);
}

function callback(results, status) {
  if (status === google.maps.places.PlacesServiceStatus.OK) {
    for (var i = 0; i < results.length; i++) {
      createMarker(results[i]);
	 // alert(results[i].name);
    }
  }
}

function createMarker(place) {
  var placeLoc = place.geometry.location;
  var marker = new google.maps.Marker({
    map: map,
    position: place.geometry.location
  });

  google.maps.event.addListener(marker, 'click', function() {
      alert(place.name+"        "+place.geometry.location.lat()+"   "+place.geometry.location.lng());
    infowindow.setContent(place.name);
    infowindow.open(map, this);
  });
}

    </script>
    
 

	
</head>
<body onLoad="getLocation()">
		<div id="tabs">
            
        </div> 
        <br /><br /><br /><br />
        <div id="container">
        	<div class="contents">
            	<center>
                    <form method="post">
                      <table>
                          <tr><br><br><br><br><br><br>
                        	<td>Search Here : </td>            
                                <td><input name="search" type="text" id="search" required="true" list="keyword" />
                              
                               <%
                                try
                                {   
                                       String mail = request.getParameter("email1");
                                         dbconne db=new dbconne(); 
                                        String s1 = "select * from statelist where city_name = '"+mail+"'";
                                      db.select(s1);
                                   /* Statement st=db.con.createStatement();
                                    String email=session.getAttribute("user").toString();
                                    ResultSet rs=st.executeQuery("select * from current_location where email='"+email+"' order by id desc");*/
                                    if(db.rs.next())
                                    {
                                        %>
                                        <input type="hidden" name="lat" id="lat" value="<%=db.rs.getString(3)%>"/>
                                        <input type="hidden" name="log" id="log" value="<%=db.rs.getString(4)%>"/>
                                        <input type="hidden" name="place" id="place" value = "s1"/>
                                        <%
                                    }
                                    else
                                    {
                                        %>
                                        Place :<input type="text" name="place" id="place"/>
                                        <%
                                    }
                                            
                                }
                                catch(Exception e)
                                {
                                    System.out.println("error  "+e);
                                }
                                
                                %>
                                <datalist id="keyword">
                                    <option value="accounting">
<option value="airport">
<option value="amusement_park">
<option value="aquarium">
<option value="art_gallery">
<option value="atm">
<option value="bakery">
<option value="bank">
<option value="bar">
<option value="beauty_salon">
<option value="bicycle_store">
<option value="book_store">
<option value="bowling_alley">
<option value="bus_station">
<option value="cafe">
<option value="campground">
<option value="car_dealer">
<option value="car_rental">
<option value="car_repair">
<option value="car_wash">
<option value="casino">
<option value="cemetery">
<option value="church">
<option value="city_hall">
<option value="clothing_store">
<option value="convenience_store">
<option value="courthouse">
<option value="dentist">
<option value="department_store">
<option value="doctor">
<option value="electrician">
<option value="electronics_store">
<option value="embassy">
<option value="establishment">
<option value="finance">
<option value="fire_station">
<option value="florist">
<option value="food">
<option value="funeral_home">
<option value="furniture_store">
<option value="gas_station">
<option value="general_contractor">
<option value="grocery_or_supermarket">
<option value="gym">
<option value="hair_care">
<option value="hardware_store">
<option value="health">
<option value="hindu_temple">
<option value="home_goods_store">
<option value="hospital">
<option value="insurance_agency">
<option value="jewelry_store">
<option value="laundry">
<option value="lawyer">
<option value="library">
<option value="liquor_store">
<option value="local_government_office">
<option value="locksmith">
<option value="lodging">
<option value="meal_delivery">
<option value="meal_takeaway">
<option value="mosque">
<option value="movie_rental">
<option value="movie_theater">
<option value="moving_company">
<option value="museum">
<option value="night_club">
<option value="painter">
<option value="park">
<option value="parking">
<option value="pet_store">
<option value="pharmacy">
<option value="physiotherapist">
<option value="place_of_worship">
<option value="plumber">
<option value="police">
<option value="post_office">
<option value="real_estate_agency">
<option value="restaurant">
<option value="roofing_contractor">
<option value="rv_park">
<option value="school">
<option value="shoe_store">
<option value="shopping_mall">
<option value="spa">
<option value="stadium">
<option value="storage">
<option value="store">
<option value="subway_station">
<option value="synagogue">
<option value="taxi_stand">
<option value="train_station">
<option value="ravel_agency">
<option value="university">
<option value="veterinary_care">
<option value="zoo">
                     </datalist>
                            </td>
                       
                        	
                          
                            <td><input type="submit" name="Submit" value="Submit" /></td>
                        </tr>
                    </table>
                        
                        <%
                        if(request.getParameter("Submit")!=null)
                        {
                            %>
                             <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyBVXwgSfvHXXP-VibYL4XgyeMzagyJnEdk&signed_in=true&libraries=places&callback=initMap" async defer></script>
                            <%
                        }
                        else
                        {
                            %>
                            <script>
                                
                            </script>
                            <%
                        }
                        
                        %>
                    </form>
                    <div id="map">
					
					
					</div>
                    
                    
                 
            	</center>
            </div>
        </div>
</body>
</html>