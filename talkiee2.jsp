<%-- 
    Document   : talkiee2
    Created on : 28 Mar, 2017, 12:40:49 PM
    Author     : akhil
--%>

<%@page import="dbclass.dbconne"%>
<%@page import="FreeTTS.FreeTTS"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>

<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSP Page</title>
    </head>
    <style>
        
     </style>   
     <meta charset="utf-8">
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
        <link rel="icon" type="image/png" href="images/favicon.png">
        <title>Banking bot</title>
        <meta name="description" content="">
        <meta name="keywords" content="">
        <meta name="author" content="">
        <!-- Mobile Specific Metas
        ================================================== -->
        <meta name="format-detection" content="telephone=no">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        
        <!-- Template CSS Files
        ================================================== -->
        <!-- Twitter Bootstrs CSS -->
        <link rel="stylesheet" href="css/bootstrap.min.css">
        <!-- Ionicons Fonts Css -->
        <link rel="stylesheet" href="css/ionicons.min.css">
        <!-- animate css -->
        <link rel="stylesheet" href="css/animate.css">
        <!-- Hero area slider css-->
        <link rel="stylesheet" href="css/slider.css">
        <link rel="stylesheet" href="css/table.css">
        <!-- owl craousel css -->
        <link rel="stylesheet" href="css/owl.carousel.css">
        <link rel="stylesheet" href="css/owl.theme.css">
        <link rel="stylesheet" href="css/jquery.fancybox.css">
        <!-- template main css file -->
        <link rel="stylesheet" href="css/main.css">
        <!-- responsive css -->
        <link rel="stylesheet" href="css/responsive.css">
        
        <!-- Template Javascript Files
        ================================================== -->
        <!-- modernizr js -->
        <script src="js/vendor/modernizr-2.6.2.min.js"></script>
        <!-- jquery -->
        <script src="//ajax.googleapis.com/ajax/libs/jquery/1.10.2/jquery.min.js"></script>
        <!-- owl carouserl js -->
        <script src="js/owl.carousel.min.js"></script>
        <!-- bootstrap js -->

        <script src="js/bootstrap.min.js"></script>
        <!-- wow js -->
        <script src="js/wow.min.js"></script>
        <!-- slider js -->
        <script src="js/slider.js"></script>
        <script src="js/jquery.fancybox.js"></script>
        <!-- template main js -->
        <script src="js/main.js"></script>
    
    <body>
        <header id="top-bar" class="navbar-fixed-top animated-header">
            <div class="container">
                <div class="navbar-header">
                    <!-- responsive nav button -->
                    <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    </button>
                    <!-- /responsive nav button -->
                    
                    <!-- logo -->
                    <div class="navbar-brand">
                        <a href="index.html" >
                            <img src="images/bba.png" alt="">
                        </a>
                    </div>
                    <!-- /logo -->
                </div>
                <!-- main menu -->
                <nav class="collapse navbar-collapse navbar-right" role="navigation">
                    <div class="main-menu">
                        <ul class="nav navbar-nav navbar-right">
                           <li><a href="about.html">About </a></li>
                           <li><a href="contact.html">Contact </a></li>
                           
                            <li><a href="feedback.jsp">FeedBack</a></li>
                            <li><a href="newsfeed.jsp">LatestNews</a></li>
                            <li><a href ="chat/chat.jsp">Chat here</a></li>
                            <li><a href ="branchlocator/map2.jsp">Bank Locator </a><li>
                            <li><a href ="logout.jsp">Logout</a><li>   
                            
                        </ul>
                    </div>
                </nav>
                <!-- /main nav -->
            </div>
        </header>
        <br><br><br><br><br><br><br>
    <center>
       <form action ="#" method ="post">
      
           <h4>Select the bank of your choice<select name="bank">
  <option value="AXIS">AXIS</option>
  <option value="ICICI">ICICI</option>
  <option value="HDFC">HDFC</option>
  <option value="SBI">SBI</option>
    
               </select>
             
<button type="submit" class="btn btn-default" name = "submit">Submit</button>
       </form> </center> </h4>
  
        
        
<%
    try
{
    dbconne db = new dbconne();
 if(request.getParameter("submit")!=null)
        {
            String banking=request.getParameter("bank");
            if(request.getParameter("bank").contains("AXIS")){
            String sql= "select * from tbl_axis";
            db.select(sql);

            }
            else if(request.getParameter("bank").contains("ICICI"))
            {
               String sql1= "select * from tbl_icici";
            db.select(sql1); 
            }
             else if(request.getParameter("bank").contains("HDFC"))
            {
               String sql2= "select * from tbl_hdfc";
            db.select(sql2); 
            }
             else if(request.getParameter("bank").contains("SBI"))
            {
               String sql3= "select * from tbl_sbi";
            db.select(sql3); 
            }
             else if(request.getParameter("bank").contains("SBT"))
            {
               String sql4= "select * from tbl_sbt";
            db.select(sql4); 
            }
        
        
  %>
  
  <table style="margin: 4px auto;">
      <caption><center><h2>Topics</h2></center></caption>
            <tr>
               
                <th>Topic</th>
                <th>Details</th>
                </tr>
<%   while(db.rs.next())
                           {
                          
%>
<tr><td><a href="speech.jsp?id=<%=db.rs.getString("Id")%>&banking=<%=banking%>"><%=db.rs.getString("topic")%></a></td>
<td><%=db.rs.getString("details")%></td></tr>    
    <%    
            

}       }

} catch(Exception e)
                {
                out.println(e);
                }

    

   
%>

               
               
             
    
         
     
    
    
    </body>
</html>
